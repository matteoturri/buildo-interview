package dao;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;

import dao.ConfigurationModule;
import dao.IConfigurationDAO;
import models.Configuration;

public class ConfigurationDAOTest {

	private static Injector injector;
	private static IConfigurationDAO dao;

	private static final String FOO_ID = "foo";
	private static final String FOO_NAME = "Configuration for Foo";
	private static final String FOO_VALUE = "This is the value for configuration Foo";

	private static final String BAR_ID = "bar";
	private static final String BAR_NAME = "Configuration for Bar";
	private static final String BAR_VALUE = "This is the value for configuration Bar";

	@BeforeClass
	public static void before(){
		injector = Guice.createInjector(new ConfigurationModule());
		dao = injector.getInstance(IConfigurationDAO.class);
	}

	@Test
	public void testSave() {
		dao.clearConfigurations();
		Configuration configuration = new Configuration(FOO_ID, FOO_NAME, FOO_VALUE);
		dao.save(configuration);
		List<Configuration> listConfig = dao.findAll();
		assertTrue(listConfig.size()==1);
		assertEquals(FOO_ID, listConfig.get(0).getId());
		assertEquals(FOO_NAME, listConfig.get(0).getName());
		assertEquals(FOO_VALUE, listConfig.get(0).getValue());
	}

	@Test
	public void testDelete() {
		dao.clearConfigurations();
		Configuration configuration = new Configuration(FOO_ID, FOO_NAME, FOO_VALUE);
		dao.save(configuration);
		dao.delete(configuration);
		List<Configuration> listConfig = dao.findAll();
		assertTrue(listConfig.size()==0);
	}

	@Test
	public void testFindById() {
		dao.clearConfigurations();
		Configuration foo = new Configuration(FOO_ID, FOO_NAME, FOO_VALUE);
		dao.save(foo);
		Configuration bar = new Configuration(BAR_ID, BAR_NAME, BAR_VALUE);
		dao.save(bar);
		Optional<Configuration> optional = dao.findById(FOO_ID);
		if (optional.isPresent()){
			assertEquals(FOO_ID, optional.get().getId());
			assertEquals(FOO_NAME, optional.get().getName());
			assertEquals(FOO_VALUE, optional.get().getValue());
		}
		else{
			fail();
		}
	}

}
