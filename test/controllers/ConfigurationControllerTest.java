package controllers;

import static org.junit.Assert.*;
import org.junit.Test;

import models.Configuration;
import play.libs.Json;
import play.mvc.Call;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;

import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;

public class ConfigurationControllerTest extends WithApplication {

	private static final String FOO_ID = "foo";
	private static final String FOO_NAME = "name for foo";
	private static final String FOO_VALUE = "value for foo";
	private static final String FOO_UPDATED_NAME = "updated name for foo";
	private static final String FOO_UPDATED_VALUE = "updated value for foo";
	private static final String NON_EXISTING_ID = "doesnotexist";

	/**
	 * Check that listAll returns an empty json object when no data is stored.
	 */
	@Test
	public void testListAllEmpty() {
		Helpers.running(Helpers.fakeApplication(), () -> {
			Call action = controllers.routes.ConfigurationController.listAll();
			Result result = Helpers.route(Helpers.fakeRequest(action));
			assertEquals(OK, result.status());
			assertEquals("application/json", result.contentType().get());
			assertTrue(contentAsString(result).equals("[]"));
		});
	}

	/**
	 * Store a configuration and check that it is returned by listAll action.
	 */
	@Test
	public void testStoreAndListAll() {
		Helpers.running(Helpers.fakeApplication(), () -> {
			Configuration foo = new Configuration(FOO_ID, FOO_NAME, FOO_VALUE);
			Call storeAction = controllers.routes.ConfigurationController.store(FOO_ID);
			Result result = Helpers.route(Helpers.fakeRequest(storeAction).bodyJson(Json.toJson(foo)));
			assertEquals(OK, result.status());
			assertEquals("text/plain", result.contentType().get());
			assertTrue(contentAsString(result).equals("Configuration stored"));
			Call listAllAction = controllers.routes.ConfigurationController.listAll();
			Result listAllResult = Helpers.route(Helpers.fakeRequest(listAllAction));
			assertEquals(OK, listAllResult.status());
			assertEquals("application/json", listAllResult.contentType().get());
			assertTrue(contentAsString(listAllResult).contains(FOO_ID));
			assertTrue(contentAsString(listAllResult).contains(FOO_NAME));
			assertTrue(contentAsString(listAllResult).contains(FOO_VALUE));
		});
	}

	/**
	 * Store a configuration and check that it is returned by read action.
	 */
	@Test
	public void testStoreAndRead() {
		Helpers.running(Helpers.fakeApplication(), () -> {
			Configuration foo = new Configuration(FOO_ID, FOO_NAME, FOO_VALUE);
			Call storeAction = controllers.routes.ConfigurationController.store(FOO_ID);
			Helpers.route(Helpers.fakeRequest(storeAction).bodyJson(Json.toJson(foo)));
			Call readAction = controllers.routes.ConfigurationController.read(FOO_ID);
			Result result = Helpers.route(Helpers.fakeRequest(readAction));
			assertEquals(OK, result.status());
			assertEquals("application/json", result.contentType().get());
			assertTrue(contentAsString(result).contains(FOO_ID));
			assertTrue(contentAsString(result).contains(FOO_NAME));
			assertTrue(contentAsString(result).contains(FOO_VALUE));
		});
	}

	/**
	 * Store a configuration, update it and check that the updated value is returned by listAll action.
	 */
	@Test
	public void testUpdateAndListAll() {
		Helpers.running(Helpers.fakeApplication(), () -> {
			Configuration foo = new Configuration(FOO_ID, FOO_NAME, FOO_VALUE);
			Call storeAction = controllers.routes.ConfigurationController.store(FOO_ID);
			Helpers.route(Helpers.fakeRequest(storeAction).bodyJson(Json.toJson(foo)));
			Configuration updatedFoo = new Configuration(FOO_ID, FOO_UPDATED_NAME, FOO_UPDATED_VALUE);
			Call updateAction = controllers.routes.ConfigurationController.update(FOO_ID);
			Result updateResult = Helpers.route(Helpers.fakeRequest(updateAction).bodyJson(Json.toJson(updatedFoo)));
			assertEquals(OK, updateResult.status());
			assertEquals("text/plain", updateResult.contentType().get());
			assertTrue(contentAsString(updateResult).equals("Configuration updated"));
			Call listAllAction = controllers.routes.ConfigurationController.listAll();
			Result listAllResult = Helpers.route(Helpers.fakeRequest(listAllAction));
			assertEquals(OK, listAllResult.status());
			assertEquals("application/json", listAllResult.contentType().get());
			assertTrue(contentAsString(listAllResult).contains(FOO_ID));
			assertTrue(contentAsString(listAllResult).contains(FOO_UPDATED_NAME));
			assertTrue(contentAsString(listAllResult).contains(FOO_UPDATED_VALUE));
		});
	}

	/**
	 * Store a configuration, delete it and check that an empty json object is returned by listAll action.
	 */
	@Test
	public void testDelete() {
		Helpers.running(Helpers.fakeApplication(), () -> {
			Configuration foo = new Configuration(FOO_ID, FOO_NAME, FOO_VALUE);
			Call storeAction = controllers.routes.ConfigurationController.store(FOO_ID);
			Helpers.route(Helpers.fakeRequest(storeAction).bodyJson(Json.toJson(foo)));
			Call deleteAction = controllers.routes.ConfigurationController.delete(FOO_ID);
			Result deleteResult = Helpers.route(Helpers.fakeRequest(deleteAction));
			assertEquals(OK, deleteResult.status());
			assertEquals("text/plain", deleteResult.contentType().get());
			assertTrue(contentAsString(deleteResult).equals("Configuration deleted"));
			Call listAllAction = controllers.routes.ConfigurationController.listAll();
			Result listAllResult = Helpers.route(Helpers.fakeRequest(listAllAction));
			assertEquals(OK, listAllResult.status());
			assertEquals("application/json", listAllResult.contentType().get());
			assertTrue(contentAsString(listAllResult).equals("[]"));
		});
	}

	/**
	 * Store a configuration and then try to store it again, checking for a BAD REQUEST response
	 */
	@Test
	public void testStoreTwoTimes() {
		Helpers.running(Helpers.fakeApplication(), () -> {
			Configuration foo = new Configuration(FOO_ID, FOO_NAME, FOO_VALUE);
			Call storeAction = controllers.routes.ConfigurationController.store(FOO_ID);
			Result storeResult = Helpers.route(Helpers.fakeRequest(storeAction).bodyJson(Json.toJson(foo)));
			assertEquals(OK, storeResult.status());
			assertEquals("text/plain", storeResult.contentType().get());
			assertTrue(contentAsString(storeResult).equals("Configuration stored"));
			Call secondStoreAction = controllers.routes.ConfigurationController.store(FOO_ID);
			Result secondStoreResult = Helpers.route(Helpers.fakeRequest(secondStoreAction).bodyJson(Json.toJson(foo)));
			assertEquals(BAD_REQUEST, secondStoreResult.status());
			assertEquals("text/plain", secondStoreResult.contentType().get());
			assertTrue(contentAsString(secondStoreResult).equals("Configuration with id "+FOO_ID+" is already existing"));
		});
	}

	/**
	 * Read a non existing configuration, checking for a NOT FOUND response
	 */
	@Test
	public void testReadNonExisting() {
		Helpers.running(Helpers.fakeApplication(), () -> {
			Call readAction = controllers.routes.ConfigurationController.read(NON_EXISTING_ID);
			Result result = Helpers.route(Helpers.fakeRequest(readAction));
			assertEquals(NOT_FOUND, result.status());
			assertEquals("text/plain", result.contentType().get());
			assertTrue(contentAsString(result).equals("Configuration with id "+NON_EXISTING_ID+" not found"));
		});
	}

	/**
	 * Update a non existing configuration, checking for a NOT FOUND response
	 */
	@Test
	public void testUpdateNonExisting() {
		Helpers.running(Helpers.fakeApplication(), () -> {
			Call updateAction = controllers.routes.ConfigurationController.update(NON_EXISTING_ID);
			Result result = Helpers.route(Helpers.fakeRequest(updateAction));
			assertEquals(NOT_FOUND, result.status());
			assertEquals("text/plain", result.contentType().get());
			assertTrue(contentAsString(result).equals("Configuration with id "+NON_EXISTING_ID+" not found"));
		});
	}

	/**
	 * Delete a non existing configuration, checking for a NOT FOUND response
	 */
	@Test
	public void testDeleteNonExisting() {
		Helpers.running(Helpers.fakeApplication(), () -> {
			Call deleteAction = controllers.routes.ConfigurationController.delete(NON_EXISTING_ID);
			Result result = Helpers.route(Helpers.fakeRequest(deleteAction));
			assertEquals(NOT_FOUND, result.status());
			assertEquals("text/plain", result.contentType().get());
			assertTrue(contentAsString(result).equals("Configuration with id "+NON_EXISTING_ID+" not found"));
		});
	}


}
