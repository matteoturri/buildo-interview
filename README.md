**Buildo coding test**

_How can I run the service?_

The service has been built using Play Framework, which requires java and sbt (https://www.playframework.com/documentation/2.5.x/Installing)  
To run the service, just type `sbt run`

_What methods does the API expose?_

They are listed in conf/routes: the API exposes 5 methods  

HTTP Method | Path | Controller Method | Input
------------| -----|-------------------|-----------------------------------------------------
GET         | /    | listAll()         | none          
GET         | /:id | read(String id)   | id from path   
POST        | /:id | store(String id)  | id from path, name and value from JSON request body
PUT         | /:id | update(String id) | id from path, name and value from JSON request body
DELETE      | /:id | delete(String id) | id from path

listAll and read return JSON objects   
store, update and delete return plain text ok messages   
error messages are always returned as plain text   

_How has the service been tested?_

Two Junit test has been created, they can be found in the test folder   
On top of that, manual testing has been performed with Postman request that can be found at this link https://www.getpostman.com/collections/3e3028a81a33d901d13f
