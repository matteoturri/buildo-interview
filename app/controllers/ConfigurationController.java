package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;

import java.util.Optional;


import dao.IConfigurationDAO;
import models.Configuration;
import play.libs.Json;
import play.mvc.*;

public class ConfigurationController extends Controller {
	
	private static final String NAME = "name";
	private static final String VALUE = "value";

	@Inject
	private IConfigurationDAO dao;

	/**
	 * Returns all the configurations stored in memory
	 */
	public Result listAll() {
		return ok(Json.toJson(dao.findAll()));
	}

	/**
	 * Returns the configuration with the passed id if existing
	 */
	public Result read(String id){
		Optional<Configuration> optional = dao.findById(id);
		if (optional.isPresent()){
			return ok(Json.toJson(optional.get()));
		}
		else{
			return Results.notFound("Configuration with id "+id+" not found");
		}
	}

	/**
	 * Stores the configuration received in input, if not already present in memory
	 */
	public Result store(String id){
		JsonNode json = request().body().asJson();
		String name = json.findPath(NAME).textValue();
		String value = json.findPath(VALUE).textValue();
		Configuration configuration = new Configuration(id, name, value);
		Optional<Configuration> optional = dao.findById(id);
		if (!optional.isPresent()){
			dao.save(configuration);
			return Results.ok("Configuration stored");
		}
		else{
			return Results.badRequest("Configuration with id "+id+" is already existing");
		}
	}

	/**
	 * Updates the configuration with the passed id if existing
	 */
	public Result update(String id){
		Optional<Configuration> optional = dao.findById(id);
		if (optional.isPresent()){
			dao.delete(optional.get());
			JsonNode json = request().body().asJson();
			String name = json.findPath(NAME).textValue();
			String value = json.findPath(VALUE).textValue();
			Configuration configuration = new Configuration(id, name, value);
			dao.save(configuration);
			return Results.ok("Configuration updated");
		}
		else{
			return Results.notFound("Configuration with id "+id+" not found");
		}
	}
	
	/**
	 * Deletes the configuration with the passed id if existing
	 */
	public Result delete(String id){
		Optional<Configuration> optional = dao.findById(id);
		if (optional.isPresent()){
			dao.delete(optional.get());
			return ok("Configuration deleted");
		}
		else{
			return Results.notFound("Configuration with id "+id+" not found");
		}
	}

}
