package dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import models.Configuration;

public class ConfigurationDAO implements IConfigurationDAO{
	
	private Map<String, Configuration> configurations = new HashMap<String, Configuration>();
	
	public void save(Configuration configuration){
		configurations.put(configuration.getId(), configuration);
	}

	public void delete(Configuration configuration){
		configurations.remove(configuration.getId());
	}
	
	public Optional<Configuration> findById(String id){
		return Optional.ofNullable(configurations.get(id));
	}
	
	public List<Configuration> findAll(){
		return new ArrayList<Configuration>(configurations.values());
	}
	
	public void clearConfigurations(){
		configurations.clear();
	}

}
