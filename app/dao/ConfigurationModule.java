package dao;

import com.google.inject.AbstractModule;;

public class ConfigurationModule extends AbstractModule{
	
	@Override
	protected void configure(){
		bind(IConfigurationDAO.class).to(ConfigurationDAO.class).asEagerSingleton();
	}

}
