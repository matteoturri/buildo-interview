package dao;

import java.util.List;
import java.util.Optional;

import models.Configuration;

public interface IConfigurationDAO {
	
	public void save(Configuration configuration);

	public void delete(Configuration configuration);
	
	public Optional<Configuration> findById(String id);
	
	public List<Configuration> findAll();
	
	public void clearConfigurations();
}
